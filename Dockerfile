FROM stevedore-repo.oit.duke.edu/el7-nginx:latest
LABEL maintainer needs_maintainer

ADD tests.d/* /tests.d/
ADD extract/ /var/www/html/
