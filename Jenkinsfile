pipeline {
  agent {
    node {
      label 'atomic'
    }
  }
  stages {
    stage ('Build Builder Image') {
      steps {
        sh '''
          source /build/pipeline_vars
          docker build -t apidocs-builder -f Dockerfile-builder .
        '''
      }
    }
    stage ('Extract Source files') {
      steps {
        sh '''
          source /build/pipeline_vars
          docker create --name builder apidocs-builder
          docker cp builder:/build/dist ./extract
        '''
      }
    }
    stage ('Build Server Image') {
      steps {
        sh '''
          source /build/pipeline_vars
          build
        '''
      }
    }
    stage ('Cleanup non-standard build artifacts') {
      steps {
        sh '''
          source /build/pipeline_vars
          docker rm builder
          docker rmi apidocs-builder
        '''
      }
    }
    stage('Test') {
      steps {
        parallel (
          Integrity: {
          sh '''
             source /build/pipeline_vars
             test
          '''
         },
         SysSecurity: {
             echo "Placeholder"
         }
       )
      }
    }
    stage('Deploy') {
      steps {
        sh '''
           source /build/pipeline_vars
           tag
           '''
      }
    }
  }
  post {
      always {
          sh '''
             source /build/pipeline_vars
             cleanup_images
             '''
      }
      failure {
        mail(from: "doozer@build.docker.oit.duke.edu",
             to: "victor.xw@duke.edu, christopher.collins@duke.edu",
             subject: "API Docs Build Pipeline Failed",
             body: "Something failed with the Colab API Docs image build: https://build.docker.oit.duke.edu/blue/organizations/jenkins/colab%2Fapidocs/activity")
      }
  }
}
